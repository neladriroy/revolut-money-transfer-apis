
# Revolut Money Transfer APIs

Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.

 - Explicit requirements:
   
   1. You can use Java or Kotlin.
   
   2. Keep it simple and to the point (e.g. no need to implement any authentication).
   
   3. Assume the API is invoked by multiple systems and services on behalf of end users.
   
   4. You can use frameworks/libraries if you like (except Spring), but don't forget about
   
   requirement #2 and keep it simple and avoid heavy frameworks.
   
   5. The datastore should run in-memory for the sake of this test.
   
   6. The final result should be executable as a standalone program (should not require a
   
   pre-installed container/server).
   
   7. Demonstrate with tests that the API works as expected.

- Implicit requirements:

	1. The code produced by you is expected to be of high quality.

	2. There are no detailed requirements, use common sense.


## Description

  

Java RESTFul API(s) for transferring money between two account.

Objects:
Customer
CustomerAccount

## How to Run 

mvn exec:java

For Demo, application will run on Jetty Server on localhost  port 8080 as defined and  for testing the server will run on localhost port 8084.
An H2 in memory database will be initialised with few sample data fetched from Demo SQL inside the package.

Few Sample Request and Responses, 

All Customer Details:
Type: @GET
Request:

    http://localhost:8080/customer/allCustomers

Response:

    [   
    {
    "firstName": "neladri",
    "lastName": "roy",
    "userId": 1,
    "username": "neladrir",
    "emailId": "neladrir@gmail.com"
    },
    {
    "firstName": "revolut",
    "lastName": "london",
    "userId": 2,
    "username": "revlondon",
    "emailId": "revlondon@revolut.com"
    }
    ]

All Customer Accounts:
Type: @GET

    http://localhost:8080/customerAccount/allAccounts

Response:

    [
    {
    "accountId": 1,
    "userName": "revlondon",
    "accountBalance": 200.0000,
    "currencyCode": "USD"
    },
    {
    "accountId": 2,
    "userName": "neladrir",
    "accountBalance": 300.0000,
    "currencyCode": "USD"
    }
    ]

 Sample Requests
 Transactions: 
 

       http://localhost:8080/transaction

 Request: 
 

     {
    	"amount":10.0000,
    	"fromAccount":1,
    	"toAccount":2,
    	"currencyCode":"USD"
    }
    
CustomerAccount: 
 

       http://localhost:8080/customer/create

 Request: 
 

    {  
      "firstName":"ping",
      "lastName":"roy",
      "emailId":"royri@gmail.com",
      "username":"pingpong"
    }


CustomerAccount: 
 

       http://localhost:8080/customerAccount/create

 Request: 
 

     {
    "userAccountId": 42,
    "userName": "neladri",
    "accountBalance": 190.0000,
    "currencyCode": "INR"
    }
     

 


## Available APIs/Services



| HTTP Method    |PATH                          |DESCRIPTION                         |
|----------------|-------------------------------|-----------------------------|
|GET|`/customer/allCustomers`            |list of all customers            |
|POST          |`/customer/create`            |create a new customer            |
|GET          |`/customer/{username}`| get customer by username
|PUT			|`/customer/{userId}`|update customer by userId           |
|DELETE          |`/customer/{userId}`           |delete customer by id            |
|GET          |`/customer/{userId}`| get customer by userId
|GET|`customerAccount/allAccounts`            | all customers accounts           |
|GET          |`/customerAccount/{accountId}`| customer acc by userId|
|POST          |`/customerAccount/create`            |create a new customer account            |
|GET          |`customerAccount/{accountId}/balance`| get account balacne
|PUT			|`/{accountId}/deposit/{amount}`|deposit amount           |
|PUT          |`/{accountId}/withdraw/{amount}`           |withdraw amount            |
|DELETE          |`/customerAccount/{accountId}`| delete customerAccount

### Http Status

-   200 OK: The request has succeeded
-   400 Bad Request: The request could not be understood by the server
-   404 Not Found: The requested resource cannot be found
- 204 No Content : No content found
-   500 Internal Server Error: The server encountered an unexpected condition
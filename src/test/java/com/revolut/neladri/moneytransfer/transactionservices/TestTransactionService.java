package com.revolut.neladri.moneytransfer.transactionservices;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.revolut.neladri.moneytransfer.model.CustomerAccount;
import com.revolut.neladri.moneytransfer.model.CustomerTransaction;

public class TestTransactionService extends TestApplicationService{
	
	@Test
    public void testTransactionEnoughFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/transaction").build();
        BigDecimal amount = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);
        CustomerTransaction transaction = new CustomerTransaction("USD", amount, 1L, 2L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
    }

   
    @Test
    public void testTransactionNotEnoughFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/transaction").build();
        BigDecimal amount = new BigDecimal(100000).setScale(4, RoundingMode.HALF_EVEN);
        CustomerTransaction transaction = new CustomerTransaction("USD", amount, 1L, 2L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 500);
    }
    
    @Test
    public void testDeposit() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/customerAccount/1/deposit/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        CustomerAccount afterDeposit = mapper.readValue(jsonString, CustomerAccount.class);
        //check balance is increased from 150 to 250
        assertTrue(afterDeposit.getAccountBalance().equals(new BigDecimal(250).setScale(4, RoundingMode.HALF_EVEN)));
    }
    
    @Test
    public void testWithDrawSufficientFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/customerAccount/2/withdraw/50").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        CustomerAccount afterDeposit = mapper.readValue(jsonString, CustomerAccount.class);
        //check balance is decreased from 350 to 300
        assertTrue(afterDeposit.getAccountBalance().equals(new BigDecimal(300).setScale(4, RoundingMode.HALF_EVEN)));

    }
}

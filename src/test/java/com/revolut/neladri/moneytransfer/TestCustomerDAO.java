package com.revolut.neladri.moneytransfer;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.Customer;

public class TestCustomerDAO {
	private static final GetDAO h2DaoFactory = GetDAO.getDAOFactory(GetDAO.H2);
	
	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from DataSQL
		
		h2DaoFactory.populateTestData();
	}
	
	
	@Test
	public void testGetAllUsers() throws ServiceException {
		List<Customer> allUsers = h2DaoFactory.getCustomerDAO().getAllCustomers();
		System.out.println(allUsers);
		assertTrue(allUsers.size() > 1);
	}
	
	@Test
	public void testGetUserById() throws ServiceException {
		Customer u = h2DaoFactory.getCustomerDAO().getCustomerById(1L);
		assertTrue(u.getUsername().equals("neladriroyy"));
	}
	
	@Test
	public void testGetNonExistingUserById() throws ServiceException {
		Customer u = h2DaoFactory.getCustomerDAO().getCustomerById(8787878);
		assertTrue(u == null);
	}
	
	@Test
	public void testGetNonExistingUserByName() throws ServiceException {
		Customer u = h2DaoFactory.getCustomerDAO().getCustomerByName("dummyname");
		assertTrue(u == null);
	}
	
	@Test
	public void testCreateUser() throws ServiceException {
		Customer u = new Customer("Roy","TestAccount","neladrir", "roytestcustomer@gmail.com");
		long id = h2DaoFactory.getCustomerDAO().createCustomer(u);
		Customer uAfterInsert = h2DaoFactory.getCustomerDAO().getCustomerById(id);
		assertTrue(uAfterInsert.getUsername().equals("neladrir"));
		assertTrue(u.getEmailId().equals("roytestcustomer@gmail.com"));
	}
	
	@Test
	public void testUpdateUser() throws ServiceException {
		Customer u = new Customer(1L, "NameChane","TestAccount","neladriroyy", "roytestcustomer@gmail.com");
		int rowCount = h2DaoFactory.getCustomerDAO().updateCustomer(1L, u);
		// assert one row(user) updated
		assertTrue(rowCount == 1);
		assertTrue(h2DaoFactory.getCustomerDAO().getCustomerById(1L).getEmailId().equals("roytestcustomer@gmail.com"));
	}
	
	@Test
	public void testUpdateNonExistingUser() throws ServiceException {
		Customer u = new Customer(12L,"NameChane","TestAccount", "test2", "test2@gmail.com");
		int rowCount = h2DaoFactory.getCustomerDAO().updateCustomer(12L, u);
		// assert one row(user) updated
		assertTrue(rowCount == 0);
	}
}

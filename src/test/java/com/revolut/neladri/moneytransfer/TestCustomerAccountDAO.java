package com.revolut.neladri.moneytransfer;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.CustomerAccount;

public class TestCustomerAccountDAO {
	private static final GetDAO h2DaoFactory = GetDAO.getDAOFactory(GetDAO.H2);
	//private static long aid;

	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from DataSQL
		
		h2DaoFactory.populateTestData();
	}
	
	
	@Test
	public void testGetAllAccounts() throws ServiceException {
		List<CustomerAccount> allAccounts = h2DaoFactory.getAccountDAO().getAllAccount();
		System.out.println(allAccounts.size());
		System.out.println(allAccounts);
		assertTrue(allAccounts.size() >= 1);
	}
	
	@Test
	public void testGetAccountById() throws ServiceException {
		CustomerAccount account = h2DaoFactory.getAccountDAO().getAccountbyUserId(3L);
		assertTrue(account.getUserName().equals("test2"));
	}	
	
	@Test
	public void testCreateAccount() throws ServiceException {
		BigDecimal balance = new BigDecimal(100).setScale(4, RoundingMode.HALF_EVEN);
		CustomerAccount a = new CustomerAccount("test2", balance, "USD");
		long aid = h2DaoFactory.getAccountDAO().createCustomerAccount(a);
		CustomerAccount afterCreation = h2DaoFactory.getAccountDAO().getAccountbyUserId(aid);
		assertTrue(afterCreation.getUserName().equals("test2"));
		assertTrue(afterCreation.getCurrencyCode().equals("USD"));
		assertTrue(afterCreation.getAccountBalance().equals(balance));
	}
	
	@Test
	public void testUpdateAccountBalanceSufficientFund() throws ServiceException {

		BigDecimal deltaDeposit = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);
		BigDecimal afterDeposit = new BigDecimal(150).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdated = h2DaoFactory.getAccountDAO().updateAccountBalance(3L, deltaDeposit);
		assertTrue(rowsUpdated == 1);
		assertTrue(h2DaoFactory.getAccountDAO().getAccountbyUserId(3L).getAccountBalance().equals(afterDeposit));
		BigDecimal deltaWithDraw = new BigDecimal(-50).setScale(4, RoundingMode.HALF_EVEN);
		BigDecimal afterWithDraw = new BigDecimal(100).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = h2DaoFactory.getAccountDAO().updateAccountBalance(3L, deltaWithDraw);
		assertTrue(rowsUpdatedW == 1);
		assertTrue(h2DaoFactory.getAccountDAO().getAccountbyUserId(3L).getAccountBalance().equals(afterWithDraw));

	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateAccountBalanceNotEnoughFund() throws ServiceException {
		BigDecimal deltaWithDraw = new BigDecimal(-50000).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = h2DaoFactory.getAccountDAO().updateAccountBalance(3L, deltaWithDraw);
		assertTrue(rowsUpdatedW == 0);

	}
	
	
}

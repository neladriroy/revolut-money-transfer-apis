--This script is used for unit test cases, DO NOT CHANGE!

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(30) NOT NULL,
Balance DECIMAL(19,4) NOT NULL,
CurrencyCode VARCHAR(30) NOT NULL
);

CREATE UNIQUE INDEX idx_acc on Account(UserName,CurrencyCode);

INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('revlondon',200.0000,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('neladrir',300.0000,'USD');

DROP TABLE IF EXISTS User;


CREATE TABLE User (UserId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 FirstName VARCHAR(30) NOT NULL,
 LastName VARCHAR(30) NOT NULL,
 UserName VARCHAR(30) NOT NULL,
 EmailAddress VARCHAR(30) NOT NULL);

CREATE UNIQUE INDEX idx_ue on User(UserName,EmailAddress);

INSERT INTO User (FirstName, LastName, UserName, EmailAddress) VALUES ('neladri', 'roy', 'neladrir','neladrir@gmail.com');
INSERT INTO User (FirstName, LastName, UserName, EmailAddress) VALUES ('revolut','london','revlondon','revlondon@revolut.com');



package com.revolut.neladri.moneytransfer.inMemoryDB;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;

import com.revolut.neladri.moneytransfer.dao.CustomerAccountDAO;
import com.revolut.neladri.moneytransfer.dao.CustomerDAO;
import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;
import com.revolut.neladri.moneytransfer.dao_Implementation.CustomerAccountDAOImp;
import com.revolut.neladri.moneytransfer.dao_Implementation.CustomerDAOImp;

public class H2Connection extends GetDAO {

    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/test";

    static final String USER = "sa";
    static final String PASS = "";
    private final CustomerDAOImp customerDAO = new CustomerDAOImp();
	private final CustomerAccountDAOImp accountDAO = new CustomerAccountDAOImp();
    
    public H2Connection() {
    	DbUtils.loadDriver(JDBC_DRIVER);
	}
    
    public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(DB_URL, USER, PASS);

	}
    
    public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	public CustomerAccountDAO getAccountDAO() {
		return accountDAO;
	}
    
    
    
    public void populateTestData() {
		//log.info("Populating Test User Table and data ..... ");
		Connection conn = null;
		try {
			conn = H2Connection.getConnection();
			RunScript.execute(conn, new FileReader("src/test/java/resources/DataSQL.sql"));
			System.out.println("Populating Test Data");
		} catch (SQLException e) {
			//log.error("populateTestData(): Error populating user data: ", e);
			throw new RuntimeException(e);
		} catch (FileNotFoundException e) {
			//log.error("populateTestData(): Error finding test script file ", e);
			throw new RuntimeException(e);
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}

	

	
}

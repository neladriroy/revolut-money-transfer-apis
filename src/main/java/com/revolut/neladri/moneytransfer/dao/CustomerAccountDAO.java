package com.revolut.neladri.moneytransfer.dao;

import java.math.BigDecimal;
import java.util.List;

import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.CustomerAccount;
import com.revolut.neladri.moneytransfer.model.CustomerTransaction;

public interface CustomerAccountDAO {

    long createCustomerAccount(CustomerAccount account);
    CustomerAccount getAccountbyUserId(long accountId) throws ServiceException;
    List<CustomerAccount> getAllAccount() throws ServiceException;
    int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws ServiceException;
    int transferAccountBalance(CustomerTransaction customerTransaction) throws ServiceException;
	int deleteAccountById(long accountId) throws ServiceException;
}

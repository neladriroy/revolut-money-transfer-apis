package com.revolut.neladri.moneytransfer.dao;

import java.util.List;

import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.Customer;

public interface CustomerDAO {
	
	List<Customer> getAllCustomers() throws ServiceException;

	Customer getCustomerById(long userId) throws ServiceException;

	Customer getCustomerByName(String userName) throws ServiceException;
	
	/*
	 * creates a customer and generates userId
	 */
	
	long createCustomer(Customer customer) throws ServiceException;

	int updateCustomer(Long userId, Customer customer) throws ServiceException;

	int deleteCustomer(long userId) throws ServiceException;
}

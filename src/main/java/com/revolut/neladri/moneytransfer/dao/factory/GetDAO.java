package com.revolut.neladri.moneytransfer.dao.factory;

import com.revolut.neladri.moneytransfer.dao.CustomerAccountDAO;
import com.revolut.neladri.moneytransfer.dao.CustomerDAO;
import com.revolut.neladri.moneytransfer.inMemoryDB.H2Connection;

public abstract class GetDAO {
	
	public static final int H2 = 1;

	public abstract CustomerDAO getCustomerDAO();

	public abstract CustomerAccountDAO getAccountDAO();

	public abstract void populateTestData();

	public static H2Connection getDAOFactory(int factoryCode) {

		switch (factoryCode) {
		case H2:
			return new H2Connection();
		default:
			// by default using H2 in memory database
			return new H2Connection();
		}
	}

}

package com.revolut.neladri.moneytransfer.apiservices;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;
import com.revolut.neladri.moneytransfer.dao_Implementation.CustomerAccountDAOImp;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.CustomerAccount;

@Path("/customerAccount")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerAccountService {
	private final GetDAO daoFactory = GetDAO.getDAOFactory(GetDAO.H2);
	private static Logger log = Logger.getLogger(CustomerAccountService.class);
	
	@POST
    @Path("/create")
    public CustomerAccount createAccount(CustomerAccount account) throws Exception {
		final long accountId = daoFactory.getAccountDAO().createCustomerAccount(account);
        System.out.println("Account ID: " + accountId);
        
		return daoFactory.getAccountDAO().getAccountbyUserId(accountId);
    }
	
	@GET
    @Path("/{accountId}")
    public CustomerAccount getAccount(@PathParam("accountId") long accountId) throws Exception {
		return daoFactory.getAccountDAO().getAccountbyUserId(accountId);
    }
	
	@GET
	@Path("/allAccounts")
	public List<CustomerAccount> getAllAccounts() throws Exception{
		return daoFactory.getAccountDAO().getAllAccount();
	}
	
	@GET
	@Path("/{accountId}/balance")
	public BigDecimal getAccountBalance(@PathParam("accountId") long accountId) throws Exception {
		CustomerAccount customer = daoFactory.getAccountDAO().getAccountbyUserId(accountId);
		return customer.getAccountBalance();
	}
	
	/**
     * Deposit amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws CustomException
     */
    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public CustomerAccount deposit(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws ServiceException {

        if (amount.compareTo(CustomerAccountDAOImp.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

        daoFactory.getAccountDAO().updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountbyUserId(accountId);
    }
    
    
    /**
     * Withdraw amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws CustomException
     */
    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public CustomerAccount withdraw(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws ServiceException {

        if (amount.compareTo(CustomerAccountDAOImp.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw service: delta change to account  " + delta + " Account ID = " +accountId);
        daoFactory.getAccountDAO().updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountbyUserId(accountId);
    }
    
    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) throws ServiceException {
        int deleteCount = daoFactory.getAccountDAO().deleteAccountById(accountId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}


package com.revolut.neladri.moneytransfer.apiservices;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.revolut.neladri.moneytransfer.dao_Implementation.CustomerAccountDAOImp;
import com.revolut.neladri.moneytransfer.model.CustomerTransaction;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerTransactionService {
	
	/*
	 * Transfer Funds between accounts
	 */
	@POST
	public Response transferFund(CustomerTransaction transaction) throws Exception {
		CustomerAccountDAOImp customerAccount = new CustomerAccountDAOImp();
			int updateCount = customerAccount.transferAccountBalance(transaction);
			
			if (updateCount == 2) {
				return Response.status(Response.Status.OK).build();
			} else {
				// transaction failed
				throw new WebApplicationException("Transaction failed", Response.Status.BAD_REQUEST);
			}

		
	}
	
}

package com.revolut.neladri.moneytransfer.apiservices;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;
import com.revolut.neladri.moneytransfer.dao_Implementation.CustomerDAOImp;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.model.Customer;

@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerService {
	private final GetDAO daoFactory = GetDAO.getDAOFactory(GetDAO.H2);
	
	/*
	 * Get All Customers
	 * Sample Data Loaded from /test/java/resources/DataSQL.sql 
	 */
	@GET
	@Path("/allCustomers")
	public List<Customer> getAllCustomers() throws ServiceException{
		return daoFactory.getCustomerDAO().getAllCustomers();
	}
	
	/*
	 * Create A new customer
	 */
	@POST
	@Path("/create")
	public Customer createCustomer(Customer customer) throws ServiceException{
		final long userId = daoFactory.getCustomerDAO().createCustomer(customer);
        System.out.println("Account ID: " + userId);
		return daoFactory.getCustomerDAO().getCustomerById(userId);
	}
	
	/*
	 * Get Customer Data by UserName
	 */
	
	@GET
	@Path("/{userName}")
	public Customer getCustomerByName(@PathParam("userName") String userName) throws ServiceException {
		return daoFactory.getCustomerDAO().getCustomerByName(userName);
		
	}
	
	/*
	 * Delete A Customer by providing the userID
	 */
	
	@DELETE
	@Path("/{userId}")
	public int deleteCustomer(@PathParam("userId")long userId) throws ServiceException {
		return daoFactory.getCustomerDAO().deleteCustomer(userId);
		
	}
	
	/*
	 * Update Customer EmailAddess or UserName 
	 */
	
	@PUT
	@Path("/{userId}")
	public Customer updateCustomer(@PathParam("userId") long userId, Customer customer) throws ServiceException {
		daoFactory.getCustomerDAO().updateCustomer(userId, customer);
		return daoFactory.getCustomerDAO().getCustomerById(userId);
	}
}

package com.revolut.neladri.moneytransfer.dao_Implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import com.revolut.neladri.moneytransfer.dao.CustomerDAO;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.inMemoryDB.H2Connection;
import com.revolut.neladri.moneytransfer.model.Customer;

public class CustomerDAOImp implements CustomerDAO{
	
	private static Logger log = Logger.getLogger(CustomerDAOImp.class);
	private final static String SQL_GET_USER_BY_ID = "SELECT * FROM User WHERE UserId = ? ";
    private final static String SQL_GET_ALL_USERS = "SELECT * FROM User";
    private final static String SQL_GET_USER_BY_NAME = "SELECT * FROM User WHERE UserName = ? ";
    private final static String SQL_INSERT_USER = "INSERT INTO User (FirstName, LastName, UserName, EmailAddress) VALUES (?, ?, ?, ?)";
    private final static String SQL_UPDATE_USER = "UPDATE User SET UserName = ?, EmailAddress = ? WHERE UserId = ? ";
    private final static String SQL_DELETE_USER_BY_ID = "DELETE FROM User WHERE UserId = ? ";

	@Override
	public List<Customer> getAllCustomers() throws ServiceException {
		Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Customer> users = new ArrayList<Customer>();
        try {
            conn = H2Connection.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_USERS);
            rs = stmt.executeQuery();
            while (rs.next()) {
            	Customer user = new Customer(rs.getLong("UserId"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("UserName"), rs.getString("EmailAddress"));
                users.add(user);
                if (log.isDebugEnabled())
                  log.debug("getAllCustomers() Retrieve User: " + user);
            }
            return users;
            
        } catch (SQLException e) {
            throw new ServiceException("Error reading user data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }
		
	

	@Override
	public Customer getCustomerById(long userId) throws ServiceException {
		Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        Customer customer = null;
        try {
            conn = H2Connection.getConnection();
            statement = conn.prepareStatement(SQL_GET_USER_BY_ID);
            statement.setLong(1, userId);
            rs = statement.executeQuery();
            
            while (rs.next()) {
            	customer = new Customer(rs.getLong("UserId"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("UserName"), rs.getString("EmailAddress"));
                
                if (log.isDebugEnabled())
                    log.debug("Retrieve Customer: " + customer);
            }
            return customer;
        } catch (SQLException e) {
            throw new ServiceException("Error reading customer data", e);
        } finally {
            DbUtils.closeQuietly(conn, statement, rs);
        }
	}

	@Override
	public Customer getCustomerByName(String userName) throws ServiceException {
		Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        Customer customer = null;
        try {
            conn = H2Connection.getConnection();
            statement = conn.prepareStatement(SQL_GET_USER_BY_NAME);
            statement.setString(1, userName);
            rs = statement.executeQuery();
            
            while (rs.next()) {
            	customer = new Customer(rs.getLong("UserId"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("UserName"), rs.getString("EmailAddress"));
                
                if (log.isDebugEnabled())
                   log.debug("getCustomerByName() Retrieve Customer: " + customer);
            }
            return customer;
        } catch (SQLException e) {
            throw new ServiceException("Error reading customer data", e);
        } finally {
            DbUtils.closeQuietly(conn, statement, rs);
        }
	}

	@Override
	public long createCustomer(Customer customer) throws ServiceException {
		Connection conn = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            conn = H2Connection.getConnection();
            statement = conn.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getUsername());
            statement.setString(4, customer.getEmailId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                log.error("insertUser(): Creating customer failed, no rows affected." + customer);
                throw new ServiceException("Customer Cannot be created");
            }
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
               log.error("insertUser():  Creating customer failed, no ID obtained." + customer);
                throw new ServiceException("Customer Cannot be created");
            }
        } catch (SQLException e) {
            log.error("Error Inserting customer :" + customer);
            throw new ServiceException("Error creating customer data", e);
            
        } finally {
            DbUtils.closeQuietly(conn,statement,generatedKeys);
        }
	}

	@Override
	public int updateCustomer(Long userId, Customer customer) throws ServiceException {
		Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = H2Connection.getConnection();
            stmt = conn.prepareStatement(SQL_UPDATE_USER);
            stmt.setString(1, customer.getUsername());
            stmt.setString(2, customer.getEmailId());
            stmt.setLong(3, userId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error("Error Updating Customer Details :" + customer);
            throw new ServiceException("Error update customer data", e);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
	}

	@Override
	public int deleteCustomer(long userId) throws ServiceException {
		Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = H2Connection.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE_USER_BY_ID);
            stmt.setLong(1, userId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error("Error Deleting Customer :" + userId);
            throw new ServiceException("Error Deleting Customer ID:"+ userId, e);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
	}

}

package com.revolut.neladri.moneytransfer.dao_Implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import com.revolut.neladri.moneytransfer.dao.CustomerAccountDAO;
import com.revolut.neladri.moneytransfer.exceptions.ServiceException;
import com.revolut.neladri.moneytransfer.inMemoryDB.H2Connection;
import com.revolut.neladri.moneytransfer.model.CustomerAccount;
import com.revolut.neladri.moneytransfer.model.CustomerTransaction;

public class CustomerAccountDAOImp implements CustomerAccountDAO {
	private static Logger log = Logger.getLogger(CustomerAccountDAOImp.class);
    private final static String SQL_CREATE_ACC = "INSERT INTO Account (UserName, Balance, CurrencyCode) VALUES (?, ?, ?)";
    private final static String SQL_GET_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? ";
    private final static String SQL_GET_ALL_ACCOUNTS = "SELECT * From Account";
    private final static String SQL_LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? FOR UPDATE";
    private final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ? ";
	private final static String SQL_DELETE_ACC_BY_ID = "DELETE FROM Account WHERE AccountId = ?";
  //zero amount with scale 4 and financial rounding mode
    public static final BigDecimal zeroAmount = new BigDecimal(0).setScale(4, RoundingMode.HALF_EVEN);
    
    /*
    * Get Customer Account by UserId*/
    
    public CustomerAccount getAccountbyUserId(long accountId) throws ServiceException  {
    	Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		CustomerAccount acc = null;
		try {
			conn = H2Connection.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ACC_BY_ID);
			stmt.setLong(1, accountId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				acc = new CustomerAccount(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("Retrieve Account By Id: " + acc);
			}
			return acc;
		} catch (SQLException e) {
			throw new ServiceException("getAccountById(): Error reading account data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
    }

/*
 * Create a new Customer Account 
 */
	public long createCustomerAccount(CustomerAccount account) {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			conn = H2Connection.getConnection();
			statement = conn.prepareStatement(SQL_CREATE_ACC);
			statement.setString(1, account.getUserName());
			statement.setBigDecimal(2, account.getAccountBalance());
			statement.setString(3, account.getCurrencyCode());
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				log.error("createAccount(): Creating account failed, no rows affected.");
				throw new Exception("Account Cannot be created");
			}
			rs = statement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				log.error("Creating account failed, no ID obtained.");
				throw new Exception("Account Cannot be created");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DbUtils.closeQuietly(conn, statement, rs);
		}
		return 0;
	}

/*
 * Get All Customer Accounts
 */
	public List<CustomerAccount> getAllAccount() throws ServiceException{
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<CustomerAccount> allAccounts = new ArrayList<>();
		try {
			conn = H2Connection.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ALL_ACCOUNTS);
			rs = stmt.executeQuery();
			while (rs.next()) {
				CustomerAccount account = new CustomerAccount(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				allAccounts.add(account);
				if (log.isDebugEnabled())
					log.debug("getAllAccounts(): Get  Account " + account);
			}
			
			return allAccounts;
		} catch (SQLException e) {
			throw new ServiceException("getAccountById(): Error reading account data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
		
		
		
		
	}
	
	public int transferAccountBalance(CustomerTransaction customerTransaction) throws ServiceException {
		int result = -1;
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		CustomerAccount fromAccount = null;
		CustomerAccount toAccount = null;
		
		try {
			conn = H2Connection.getConnection();
			conn.setAutoCommit(false);
			// lock the credit and debit account for writing:
			lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
			lockStmt.setLong(1, customerTransaction.getFromAccount());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				fromAccount = new CustomerAccount(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance from Account: " + fromAccount);
			}
			lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
			lockStmt.setLong(1, customerTransaction.getToAccount());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				toAccount = new CustomerAccount(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance to Account: " + toAccount);
			}

			// check locking status
			if (fromAccount == null || toAccount == null) {
				throw new ServiceException("Fail to lock both accounts for write");
			}

			// check for funds in source account before update
			BigDecimal fromAccountLeftOver = fromAccount.getAccountBalance().subtract(customerTransaction.getAmount());
			if (fromAccountLeftOver.compareTo(CustomerAccountDAOImp.zeroAmount) < 0) {
				throw new ServiceException("Not enough Fund from source Account ");
			
			}
			// proceed with the transaction
			updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
			updateStmt.setBigDecimal(1, fromAccountLeftOver);
			updateStmt.setLong(2, customerTransaction.getFromAccount());
			updateStmt.addBatch();
			updateStmt.setBigDecimal(1, toAccount.getAccountBalance().add(customerTransaction.getAmount()));
			updateStmt.setLong(2, customerTransaction.getToAccount());
			updateStmt.addBatch();
			int[] rowsUpdated = updateStmt.executeBatch();
			result = rowsUpdated[0] + rowsUpdated[1];
			if (log.isDebugEnabled()) {
				log.debug("Number of rows updated for the transfer : " + result);
			}
			// If there is no error, commit the transaction
			conn.commit();
		} catch (SQLException se) {
			// rollback transaction if exception occurs
			log.error("transferAccountBalance(): User Transaction Failed, rollback initiated for: " + customerTransaction,
					se);
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				throw new ServiceException("Fail to rollback transaction", re);
			}
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
			DbUtils.closeQuietly(updateStmt);
		}
		return result;
	}
	
	
	/**
	 * Update account balance
	 */
	public int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws ServiceException {
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		CustomerAccount targetAccount = null;
		int updateCount = -1;
		try {
			conn = H2Connection.getConnection();
			conn.setAutoCommit(false);
			// lock account for writing:
			lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
			lockStmt.setLong(1, accountId);
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				targetAccount = new CustomerAccount(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("updateAccountBalance from Account: " + targetAccount);
			}

			if (targetAccount == null) {
				throw new ServiceException("updateAccountBalance(): fail to lock account : " + accountId);
			}
			// update account upon success locking
			BigDecimal balance = targetAccount.getAccountBalance().add(deltaAmount);
			if (balance.compareTo(CustomerAccountDAOImp.zeroAmount) < 0) {
				throw new ServiceException("Not sufficient Fund for account: " + accountId);
			}

			updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
			updateStmt.setBigDecimal(1, balance);
			updateStmt.setLong(2, accountId);
			updateCount = updateStmt.executeUpdate();
			conn.commit();
			if (log.isDebugEnabled())
				log.debug("New Balance after Update: " + targetAccount);
			return updateCount;
		} catch (SQLException se) {
			// rollback transaction if exception occurs
			log.error("updateAccountBalance(): User Transaction Failed, rollback initiated for: " + accountId, se);
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				throw new ServiceException("Fail to rollback transaction", re);
			}
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
			DbUtils.closeQuietly(updateStmt);
		}
		return updateCount;
	}

	public int deleteAccountById(long accountId) throws ServiceException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = H2Connection.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE_ACC_BY_ID);
			stmt.setLong(1, accountId);
			return stmt.executeUpdate();
		} catch (SQLException e) {
			throw new ServiceException("deleteAccountById(): Error deleting user account Id " + accountId, e);
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(stmt);
		}
	}

}

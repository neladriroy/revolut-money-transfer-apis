package com.revolut.neladri.moneytransfer.model;

import java.math.BigDecimal;

public class CustomerTransaction {
	
	private BigDecimal amount;
	private Long fromAccount;
	private Long toAccount;
	private String currencyCode;
	public CustomerTransaction() {
		super();
	}
	
	
	public CustomerTransaction(String currencyCode, BigDecimal amount, Long fromAccount, Long toAccount) {
		super();
		this.amount = amount;
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
		this.currencyCode = currencyCode;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Long getFromAccount() {
		return fromAccount;
	}


	public void setFromAccount(Long fromAccount) {
		this.fromAccount = fromAccount;
	}


	public Long getToAccount() {
		return toAccount;
	}


	public void setToAccount(Long toAccount) {
		this.toAccount = toAccount;
	}


	public String getCurrencyCode() {
		return currencyCode;
	}


	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((currencyCode == null) ? 0 : currencyCode.hashCode());
		result = prime * result + ((fromAccount == null) ? 0 : fromAccount.hashCode());
		result = prime * result + ((toAccount == null) ? 0 : toAccount.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerTransaction other = (CustomerTransaction) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (currencyCode == null) {
			if (other.currencyCode != null)
				return false;
		} else if (!currencyCode.equals(other.currencyCode))
			return false;
		if (fromAccount == null) {
			if (other.fromAccount != null)
				return false;
		} else if (!fromAccount.equals(other.fromAccount))
			return false;
		if (toAccount == null) {
			if (other.toAccount != null)
				return false;
		} else if (!toAccount.equals(other.toAccount))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "CustomerTransaction [amount=" + amount + ", fromAccount=" + fromAccount + ", toAccount=" + toAccount
				+ ", currencyCode=" + currencyCode + "]";
	}
	
	
	
	
	

}

package com.revolut.neladri.moneytransfer.model;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class CustomerAccount {
	
	private long accountId;
	private String userName;
	private BigDecimal accountBalance;
	private String currencyCode;
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long userAccountId) {
		this.accountId = userAccountId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public BigDecimal getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public CustomerAccount() {
		super();
	}
	
	public CustomerAccount(String userName, BigDecimal accountBalance, String currencyCode) {
		super();
		this.userName = userName;
		this.accountBalance = accountBalance;
		this.currencyCode = currencyCode;
	}
	
	public CustomerAccount(long userAccountId, String userName, BigDecimal accountBalance, String currencyCode) {
		super();
		this.accountId =  userAccountId;;
		this.userName = userName;
		this.accountBalance = accountBalance;
		this.currencyCode = currencyCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CustomerAccount that = (CustomerAccount) o;
		return Objects.equals(accountId, that.accountId) &&
				Objects.equals(userName, that.userName) &&
				Objects.equals(accountBalance, that.accountBalance) &&
				Objects.equals(currencyCode, that.currencyCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountId, userName, accountBalance, currencyCode);
	}

	@Override
	public String toString() {
		return "CustomerAccount [accountId=" + accountId + ", userName=" + userName + ", accountBalance="
				+ accountBalance + ", currencyCode=" + currencyCode + "]";
	}
	
	
	
	
	
	
}

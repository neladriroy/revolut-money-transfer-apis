package com.revolut.neladri.moneytransfer.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

@Provider
	public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {
		private static Logger log = Logger.getLogger(ServiceExceptionMapper.class);

		public ServiceExceptionMapper() {
		}

		public Response toResponse(ServiceException daoException) {
			if (log.isDebugEnabled()) {
				log.debug("Mapping exception to Response....");
			}
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setErrorCode(daoException.getMessage());

			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorResponse).type(MediaType.APPLICATION_JSON).build();
		}

		
	}



package com.revolut.neladri.moneytransfer;



import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import com.revolut.neladri.moneytransfer.apiservices.CustomerAccountService;
import com.revolut.neladri.moneytransfer.apiservices.CustomerService;
import com.revolut.neladri.moneytransfer.apiservices.CustomerTransactionService;
import com.revolut.neladri.moneytransfer.dao.factory.GetDAO;


public class App 
{
	private static Logger log = Logger.getLogger(App.class);

	public static void main(String[] args) throws Exception {
		// Initialize H2 database with demo data
		log.info("Initializing MoneyTransfer Demo Data");
		GetDAO h2DaoFactory = GetDAO.getDAOFactory(GetDAO.H2);
		h2DaoFactory.populateTestData();
		
		// Services running on Jetty
		startService();
	}

	private static void startService() throws Exception {
		Server server = new Server(8080);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);
		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
		servletHolder.setInitParameter("jersey.config.server.provider.classnames",
				CustomerAccountService.class.getCanonicalName() + "," +  CustomerService.class.getCanonicalName()  + "," + CustomerTransactionService.class.getCanonicalName());
		try {
			server.start();
			server.join();
		} finally {
			server.destroy();
		}
	}
}
